`sleepd` suspends the system
and sets up hardware RTC alarms
according to requests from user programs.

Clients can set up alarms,
which will be honored
by waking up the system
before the specified time.
If the specified time is in the past,
the system will stay awake indefinitely.

`sleepd-until` allows suspending until a specified point in time.

`sleepd-xss` prevents sleep during user activity,
as indicated by the X11 Screen Saver extension.

`yt-wait` is a wrapper script for yt-dlp
that prevents sleep while downloading
and sleeps while waiting for a video to become available.

## How to sleep
The following pattern is used by `yt-wait`.

	(
		# Prepare some things. The system won't be suspended.
		echo $unix_time >&3        # Send timestamp to sleepd-until -p
		sleepd-until $unix_time
		# Do the work.
	) 3>&1 1>&2 | sleepd-until -p
	# Redirection prevents command output being sent to sleepd-until

## Dependencies
- rtcwake (from util-linux)
