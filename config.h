#define MAX_CLIENTS 128

/* The executable at after_script_path will be executed after the system wakes up,
 * but before waking up clients. Set to NULL to disable this feature. */
static char	*after_script_path = "/etc/sleepd_after";
static char	*pid_path = "/run/sleepd.pid";
static char	*socket_path = "/run/sleepd.sock";
static char	*suspend_to = "mem";

/* Following time values are in seconds. */
static int	min_sleep_time = 60;
static int	max_sleep_time = 14*24*3600;
static int	min_awake_time = 2*60;
static int	alarm_error_margin = 20;
