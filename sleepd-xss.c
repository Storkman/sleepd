#define _DEFAULT_SOURCE
#include <errno.h>
#include <poll.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include <X11/extensions/scrnsaver.h>

#define SLEEPD_IMPLEMENT_HERE
#include "sleepd.h"

Window	create_window(void);
void	die(const char*, ...);
int	read_events(void);
void	wait_for_event(int timeout);
void	xinit();

Display	*disp;
Window	root;
int	screen;
int	evnum;

unsigned int ms_after_blank = 10*60*1000;

Window
create_window(void)
{
	Window w;
	XSetWindowAttributes attr;
	attr.event_mask = ScreenSaverNotifyMask;
	w = XCreateWindow(disp, root, -2, -2, 1, 1, 0,
		DefaultDepth(disp, screen), InputOutput, DefaultVisual(disp, screen),
		CWEventMask, &attr);
	XScreenSaverSelectInput(disp, w, ScreenSaverNotifyMask);
	return w;
}

void
die(const char *fmt, ...)
{
	va_list ap;
	
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	
	if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	}
	exit(1);
}

int
main(int argc, char *argv[])
{
	XScreenSaverInfo *xssinfo;
	long wait_time;
	(void) argc; (void) argv;
	if (sleepd_open() || sleepd_introduce("X11 Screen Saver"))
		die("sleepd:");
	xinit();
	create_window();
	xssinfo = XScreenSaverAllocInfo();
	if (!xssinfo)
		die("out of memory\n");
	
	for (;;) {
		if (!XScreenSaverQueryInfo(disp, root, xssinfo))
			die("couldn't query screen saver state\n");
		switch (xssinfo->state) {
		case ScreenSaverDisabled:
		case ScreenSaverOff:
			if (sleepd_disallow())
				die("sleepd:");
			wait_for_event(-1);
			continue;
		case ScreenSaverOn:
			break;
		default:
			die("invalid screen saver state\n");
		}
		
		wait_time = ms_after_blank - xssinfo->til_or_since;
		if (wait_time > 0) {
			if (sleepd_disallow())
				die("sleepd:");
			wait_for_event(wait_time);
			continue;
		}
		
		if (sleepd_allow())
			die("sleepd:");
		wait_for_event(-1);
	}
	return 0;
}

void
wait_for_event(int timeout)
{
	XEvent ev;
	struct pollfd fds[2];
	int i, en;
	fds[0].fd = ConnectionNumber(disp);
	fds[0].events = POLLIN;
	fds[0].revents = 0;
	fds[1].fd = sleepd_fd;
	fds[1].events = POLLIN;
	fds[1].revents = 0;
	
	en = XPending(disp);
	if (timeout == 0)
		timeout = -1;
	poll(fds, 2, en ? 0 : timeout);
	en = XPending(disp);
	for (i = 0; i < en; i++)
		XNextEvent(disp, &ev);
	if (fds[1].revents) {
		long t = sleepd_read_time();
		if (t == 0)
			die("unexpected response from server\n");
		if (t < 0)
			die("read error: %s\n", strerror(-t));
	}
}

void
xinit()
{
	int errbase;
	disp = XOpenDisplay(NULL);
	if (!disp)
		die("Can't connect to X.\n");
	screen = DefaultScreen(disp);
	root = RootWindow(disp, screen);
	if (XScreenSaverQueryExtension(disp, &evnum, &errbase) != True)
		die("XScreenSaver extension not available.\n");
}
