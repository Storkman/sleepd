PREFIX = /usr/local
MANPREFIX = ${PREFIX}/share/man

_CFLAGS = --std=c89 -pedantic -Wall -Wextra -Werror=implicit ${CFLAGS}

all: sleepd sleepd-until sleepd-xss

.c.o:
	${CC} ${_CFLAGS} -c $<

tags: *.c *.h
	ctags -n *.c *.h

sleepd.o: sleepd.c config.h
sleepd: sleepd.o
	${CC} -o $@ $^

sleepd-until.o: sleepd-until.c sleepd.h
sleepd-until: sleepd-until.o
	${CC} -o $@ $^

sleepd-xss.o: sleepd-xss.c sleepd.h
sleepd-xss: sleepd-xss.o
	${CC} -o $@ $^ -L/usr/X11R6/lib -lX11 -lXext -lXss

clean:
	rm -f sleepd sleepd-until sleepd-xss *.o

install: all
	mkdir -p ${DESTDIR}${PREFIX}/bin
	cp -f sleepd sleepd-until sleepd-xss ${DESTDIR}${PREFIX}/bin
	chmod 755 ${DESTDIR}${PREFIX}/bin/{sleepd,sleepd-until,sleepd-xss}

uninstall:
	rm -f ${DESTDIR}${PREFIX}/bin/{sleepd,sleepd-until,sleepd-xss}

.PHONY: all clean install uninstall
