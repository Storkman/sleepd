/*
	sleepd.h - single-header client library for sleepd.
	
	#include <errno.h>
	#include <poll.h>
	#include <stdarg.h>
	#include <stdio.h>
	#include <stdlib.h>
	#include <string.h>
	#include <sys/socket.h>
	#include <sys/un.h>
	#include <unistd.h>
	
	#define SLEEPD_IMPLEMENT_HERE
	#include "sleepd.h"
*/

/* All functions except sleepd_read_time() return 0 on success and a negative number on failure. */
int	sleepd_open(void);	/* connect to the server */
int	sleepd_close(void);	/* disconnect */
int	sleepd_introduce(const char*);	/* set client name */
int	sleepd_allow(void);	/* set alarm far in the future */
int	sleepd_disallow(void);	/* set alarm to zero */

/* sleepd_read_time reads all pending time updates and returns the latest value.
 * Returns a negative number on error, or zero if no updates are expected.
 * This will block if waiting for an alarm. */
long	sleepd_read_time(void);

extern int	sleepd_fd;

#ifdef SLEEPD_IMPLEMENT_HERE

int	sleepd_fd;
static short	sleepd_alarm_set;

int
sleepd_allow(void)
{
	const char *tv = "2000000000\n";
	if (write(sleepd_fd, tv, strlen(tv)) < 0)
		return -1;
	++sleepd_alarm_set;
	return 0;
}

int
sleepd_close(void)
{
	if (shutdown(sleepd_fd, SHUT_RDWR) || close(sleepd_fd))
		return -errno;
	return 0;
}

int
sleepd_disallow(void)
{
	if (write(sleepd_fd, "0\n", 2) != 2)
		return -1;
	++sleepd_alarm_set;
	sleepd_read_time();
	return 0;
}

int
sleepd_introduce(const char *name)
{
	char buf[130];
	int len, n, p;
	len = snprintf(buf, sizeof(buf), "@ %s\n", name);
	if (len >= (int) sizeof(buf)) {
		errno = EINVAL;
		return -1;
	}
	p = 0;
	do {
		n = write(sleepd_fd, buf + p, len - p);
		if (n < 0)
			return -1;
		p += n;
	} while (p < len);
	sleepd_read_time();
	return 0;
}

int
sleepd_open(void)
{
	const char *path = "/run/sleepd.sock";
	struct sockaddr_un addr;
	int len;
	addr.sun_family = AF_UNIX;
	len = strlen(path);
	memmove(addr.sun_path, path, len + 1);
	sleepd_fd = socket(AF_UNIX, SOCK_STREAM, 0);
	if (connect(sleepd_fd, (const struct sockaddr*) &addr, sizeof(addr)))
		return -1;
	return 0;
}

long
sleepd_read_time(void)
{
	char buf[49];
	char *start, *end;
	long timer = 0;
	int n;
	if (!sleepd_alarm_set) {
		struct pollfd fd;
		fd.fd = sleepd_fd;
		fd.events = POLLIN;
		fd.revents = 0;
		n = poll(&fd, 1, 0);
		if (n < 0)
			return -errno;
		if (n == 0)
			/* We didn't expect a response from the server, and there's indeed none. */
			return 0;
		++sleepd_alarm_set;
	}
	while (sleepd_alarm_set > 0) {
		n = read(sleepd_fd, buf, sizeof(buf) - 1);
		if (n == 0)
			return -ECONNRESET;
		if (n < 0)
			return -errno;
		buf[n] = 0;
		start = buf;
		while (*start) {
			timer = strtol(start, &end, 10);
			if (*end != '\n')
				return -EPROTO;
			start = end + 1;
			--sleepd_alarm_set;
		}
	}
	sleepd_alarm_set = 0;
	return timer;
}

int
sleepd_until(long t)
{
	char buf[16];
	int len, n;
	if (t < 0)
		return -EINVAL;
	len = snprintf(buf, sizeof(buf), "%ld\n", t);
	n = write(sleepd_fd, buf, len);
	if (n < 0)
		return -errno;
	++sleepd_alarm_set;
	return 0;
}

#endif
