#define _DEFAULT_SOURCE
#include <assert.h>
#include <errno.h>
#include <limits.h>
#include <math.h>
#include <poll.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <time.h>

#include <fcntl.h>
#include <grp.h>
#include <pwd.h>
#include <sys/file.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/un.h>
#include <sys/wait.h>
#include <unistd.h>

#define SIZE(X) (sizeof(X)/sizeof((X)[0]))

#include "config.h"

#define BUF_SIZE 256
#define SLEEP_MSG 16
#define MAX_NAME 127

struct Buffer {
	char data[256];
	int	len;
};

typedef struct Client Client;
struct Client {
	char	name[MAX_NAME+1];
	struct Buffer	input;
	int	conn;
	int	alarm;
	bool	awake;
	bool	finished;
	Client	*next;
};

struct pollfd	pfd[MAX_CLIENTS+1];
int	nclients;
int	next_alarm;
bool	all_asleep;

void	accept_clients(void);
int	create_socket(const char*);
void	die(const char *fmt, ...);
void	drop_client(struct Client*, const char *reason);
void	drop_privs(void);
int	fork_pipe(int *input, int *output);
void	handle_commands(struct Client*);
void	handle_inputs(int nready);
int	lock_pid(void);
int	power_control(int infd, int outfd);
void	poll_input(int idx, int fd);
int	poll_wait(int timeout);
int	read_line(int fd, struct Buffer*, char *dst, int len);
void	reset_alarms(void);
void	run_script(void);
int	setgid_name(const char*);
int	setuid_name(const char*);
void	spawn_control(void);
void	suspend(int);
void	update_client_states(void);
void	update_clock(void);
void	wake_up(struct Client*);
void	write_pid(int pidfile);

struct Client	*clients;
long int	clock_val;
int	main_socket;
int	power_fd[2];
int	power_pid;

void
accept_clients(void)
{
	struct Client *c;
	int fd;
	for (;;) {
		fd = accept(main_socket, NULL, NULL);
		if (fd < 0) {
			if (errno != EAGAIN && errno != EWOULDBLOCK && errno != EINTR)
				syslog(LOG_ERR, "connection error: %s", strerror(errno));
			break;
		}
		if (fcntl(fd, F_SETFL, O_NONBLOCK)) {
			syslog(LOG_ERR, "connection error: %s", strerror(errno));
			close(fd);
			break;
		}
		c = calloc(sizeof(*c), 1);
		if (!c) {
			syslog(LOG_ERR, "dropping new client: out of memory");
			close(fd);
			break;
		}
		c->awake = true;
		c->conn = fd;
		c->next = clients;
		clients = c;
	}
}

int
create_socket(const char *path)
{
	struct sockaddr_un addr;
	mode_t mask;
	size_t len;
	int fd;
	if (unlink(path) && errno != ENOENT)
		die("couldn't create socket:");
	fd = socket(AF_UNIX, SOCK_STREAM | SOCK_NONBLOCK, 0);
	if (fd < 0)
		die("couldn't create socket:");
	addr.sun_family = AF_UNIX;
	len = strlen(path);
	assert(sizeof(addr.sun_path) > len);
	memmove(addr.sun_path, path, len + 1);
	mask = umask(0006);
	if (bind(fd, (const struct sockaddr*) &addr, sizeof(addr)) || listen(fd, 8))
		die("couldn't create socket:");
	fchmod(fd, 0660);
	umask(mask);
	return fd;
}

void
die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);

	if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	}

	abort();
}

void
drop_client(struct Client *c, const char *reason)
{
	const char *name;
	name = c->name[0] ? c->name : "<name empty>";
	syslog(LOG_ERR, "client \"%s\" dropped: %s", name, reason);
	c->awake = true;
	c->finished = true;
}

void
drop_privs(void)
{
	if (setgroups(0, NULL) < 0)
		die("setgroups:");
	if (setgid_name("nogroup") < 0)
		die("setgid:");
	if (setuid_name("nobody") < 0)
		die("setuid:");
}

int
fork_pipe(int *input, int *output)
{
	int infd[2], outfd[2];
	int err, pid;
	if (pipe(outfd)) {
		err = errno;
		perror("failed to create pipe");
		return -err;
	}
	if (pipe(infd)) {
		err = errno;
		perror("failed to create pipe");
		close(outfd[0]);
		close(outfd[1]);
		return -err;
	}
	pid = fork();
	if (pid == -1) {
		err = errno;
		perror("failed to fork");
		close(outfd[0]);
		close(outfd[1]);
		close(infd[0]);
		close(infd[1]);
		return -err;
	}
	if (pid == 0) {
		close(infd[1]);
		close(outfd[0]);
		if (input)
			*input = infd[0];
		else
			close(infd[0]);
		if (output)
			*output = outfd[1];
		else
			close(outfd[1]);
		return 0;
	}
	close(infd[0]);
	close(outfd[1]);
	if (input)
		*input = infd[1];
	else
		close(infd[1]);
	if (output)
		*output = outfd[0];
	else
		close(outfd[0]);
	return pid;
}

void
handle_commands(struct Client *c)
{
	char line[BUF_SIZE];
	char *end;
	int n, newalarm;
	for (;;) {
		n = read_line(c->conn, &c->input, line, sizeof(line));
		if (n == -EAGAIN)
			return;
		if (n == 0) {
			c->finished = true;
			return;
		}
		if (n < 0) {
			drop_client(c, strerror(-n));
			return;
		}
		
		if (line[0] == '@') {
			if (c->name[0]) {
				drop_client(c, "repeated introduction.");
				return;
			}
			if (n - 2 > MAX_NAME) {
				drop_client(c, "name too long.");
				return;
			}
			memmove(c->name, line + 2, n - 2);
			c->name[n - 2] = 0;
			continue;
		}
		
		newalarm = strtol(line, &end, 10);
		if (newalarm < 0 || *end != 0) {
			drop_client(c, "invalid message");
			return;
		}
		c->alarm = newalarm;
		c->awake = false;
	}
}

void
handle_inputs(int n)
{
	struct Client *c;
	int i;
	for (c = clients, i = 0; c; c = c->next, i++) {
		if (n == 0)
			break;
		if (!pfd[i].revents)
			continue;
		if (!c->awake)
			wake_up(c);
		else
			handle_commands(c);
		--n;
	}
}

int
lock_pid(void)
{
	int err, pf;
	
	pf = open(pid_path, O_WRONLY | O_CLOEXEC | O_CREAT | O_NOFOLLOW, 0644);
	if (pf < 0)
		die("%s: can't open:", pid_path);
	err = flock(pf, LOCK_EX | LOCK_NB);
	if (err < 0) {
		if (errno == EWOULDBLOCK)
			die("Another instance of sleepd is already running (%s locked).", pid_path);
		die("%s: can't lock:", pid_path);
	}
	
	if (fchown(pf, getuid(), getgid()))
		die("%s: can't set owner:", pid_path);
	if (fchmod(pf, 0644))
		die("%s: can't set permissions:", pid_path);
	if (ftruncate(pf, 0))
		die("%s: can't write:", pid_path);
	return pf;
}

int
main(int argc, char *argv[])
{
	struct sigaction sa;
	long sleep_allowed_after = 0;
	int ready[2];
	int n, pf, pid, sleep_time;
	(void) argc; (void) argv;
	
	pf = lock_pid();
	
	sigemptyset(&sa.sa_mask);
	sa.sa_handler = SIG_IGN;
	sa.sa_flags = SA_RESTART;	/* for good luck */
	sigaction(SIGPIPE, &sa, NULL);
	
	openlog("sleepd", LOG_NDELAY | LOG_PID, LOG_DAEMON);
	spawn_control();
	setgid_name("users");
	main_socket = create_socket(socket_path);
	drop_privs();
	
	if (pipe(ready))
		die("sleepd: pipe:");
	pid = fork();
	if (pid < 0)
		die("sleepd: fork:");
	if (pid > 0) {
		char dummy;
		close(ready[1]);
		n = read(ready[0], &dummy, 1);
		if (n < 0)
			die("sleepd: read:");
		return 0;
	}
	close(ready[0]);
	
	setsid();
	write_pid(pf);
	close(ready[1]);
	
	syslog(LOG_INFO, "sleepd started.");
	for (;;) {
		accept_clients();
		update_client_states();
		
		sleep_time = next_alarm - clock_val;
		if (sleep_time > max_sleep_time)
			sleep_time = max_sleep_time;
		
		if (clock_val < sleep_allowed_after)
			sleep_time = sleep_allowed_after - clock_val;
		else if (clients && all_asleep
		&& sleep_time - alarm_error_margin >= min_sleep_time) {
			suspend(sleep_time - alarm_error_margin);
			update_clock();
			reset_alarms();
			sleep_allowed_after = clock_val + min_awake_time;
			continue;
		}
		n = poll_wait(sleep_time);
		update_clock();
		
		handle_inputs(n);
	}
}

void
poll_input(int i, int fd)
{
	pfd[i].fd = fd;
	pfd[i].events = POLLIN;
	pfd[i].revents = 0;
}

bool
poll_ready(int i)
{
	return !!pfd[i].revents;
}

int
poll_wait(int timeout)
{
	int n;
	poll_input(nclients, main_socket);
	n = poll(pfd, nclients+1, timeout * 1000);
	if (n < 0)
		die("poll:");
	if (pfd[nclients].revents)
		n--;
	return n;
}

int
power_control(int infd, int outfd)
{
	struct timespec st, et;
	char buf[SLEEP_MSG+1];
	int nr, pid, sust;
	float d;
	for (;;) {
		nr = read(infd, &buf, SLEEP_MSG);
		if (nr == 0)
			return 0;
		if (nr < 0)
			die("read error:");
		assert(nr == SLEEP_MSG);
		buf[SLEEP_MSG] = 0;
		
		sust = atoi(buf);
		assert(sust > 0);
		syslog(LOG_INFO, "suspending to \"%s\" for %d seconds...", suspend_to, sust);
		clock_gettime(CLOCK_BOOTTIME, &st);
		pid = fork();
		if (pid < 0) {
			syslog(LOG_ERR, "fork: %s", strerror(errno));
			if (write(outfd, "\n", 1) != 1)
				die("write error:");
			continue;
		}
		if (pid == 0) {
			char tb[16];
			size_t n;
			n = snprintf(tb, sizeof(tb), "%d", sust);
			assert(n < sizeof(tb));
			execlp("rtcwake", "rtcwake", "-m", suspend_to, "-s", tb, NULL);
			abort();
		}
		waitpid(pid, NULL, 0);
		clock_gettime(CLOCK_BOOTTIME, &et);
		d = et.tv_sec - st.tv_sec + (et.tv_nsec - st.tv_nsec) * 1e-9 - sust;
		if (d > -10)
			syslog(LOG_INFO, "waking lag: %f seconds", d);
		
		run_script();
		if (write(outfd, "\n", 1) != 1)
			die("write error:");
	}
}

/* Read a buffered line from fd through buf. Returns...
 * -errno on error
 * -ENOMEM if the input line is longer than the buffer
 * -EAGAIN if a full ine is not available yet
 * 0 on EOF
 * or the number of bytes read, excluding the end-of-line character.
 */
int
read_line(int fd, struct Buffer *buf, char *dst, int cap)
{
	char *nl;
	int count, gap, n;
	nl = memchr(buf->data, '\n', buf->len);
	if (!nl) {
		gap = sizeof(buf->data) - buf->len;
		if (gap == 0)
			return -ENOMEM;
		n = read(fd, buf->data + buf->len, gap);
		if (n < 0) {
			if (errno == EAGAIN || errno == EWOULDBLOCK || errno == EINTR)
				return -EAGAIN;
			return -errno;
		}
		if (n == 0)
			return 0;
		nl = memchr(buf->data + buf->len, '\n', n);
		buf->len += n;
		if (!nl)
			return -EAGAIN;
	}
	count = nl - buf->data;
	if (count >= cap)
		return -ENOMEM;
	memmove(dst, buf->data, count);
	dst[count] = 0;
	memmove(buf->data, buf->data + count + 1, buf->len - count - 1);
	buf->len -= count + 1;
	return count;
}

void
reset_alarms(void)
{
	struct Client *c;
	for (c = clients; c; c = c->next)
		c->alarm = 0;
}

void
run_script(void)
{
	int pid, wp, ws;
	const char *path = after_script_path;
	if (!path)
		return;
	pid = fork();
	if (pid < 0) {
		syslog(LOG_ERR, "fork: %s\n", strerror(errno));
		return;
	}
	if (pid == 0) {
		execl(path, path, NULL);
		die("exec failed:");
	}
	/* Timeout would be nice. */
	for (;;) {
		wp = waitpid(pid, &ws, 0);
		if (wp > 0)
			break;
		if (wp < 0 && errno != EINTR) {
			syslog(LOG_ERR, "wait on %s [%d]: %s\n", path, pid, strerror(errno));
			return;
		}
	}
	if (WIFEXITED(ws)) {
		if (WEXITSTATUS(ws) != 0)
			syslog(LOG_WARNING, "%s: exited with status %d", path, WEXITSTATUS(ws));
	} else if (WIFSIGNALED(ws)) {
		syslog(LOG_WARNING, "%s: terminated by signal %s", path, strsignal(WTERMSIG(ws)));
	}
}

int
setgid_name(const char *name)
{
	struct group *g = getgrnam(name);
	if (!g)
		return -1;
	return setgid(g->gr_gid);
}

int
setuid_name(const char *name)
{
	struct passwd *p = getpwnam(name);
	if (!p)
		return -1;
	return setuid(p->pw_uid);
}

void
spawn_control(void)
{
	int inp, out;
	int pid = fork_pipe(&inp, &out);
	if (pid < 0)
		die("failed to fork:");
	if (pid == 0)
		exit(power_control(inp, out));
	power_fd[0] = inp;
	power_fd[1] = out;
	power_pid = pid;
}

void
suspend(int t)
{
	int n;
	char buf[SLEEP_MSG+1] = {0};
	snprintf(buf, sizeof(buf), "%d", t);
	do {
		n = write(power_fd[0], buf, SLEEP_MSG);
	} while (n == EINTR);
	if (n != SLEEP_MSG)
		die("suspend failed:");
	do {
		n = read(power_fd[1], buf, 1);
	} while (n == EINTR);
	if (n != 1)
		die("suspend failed:");
}

void
update_client_states(void)
{
	struct Client **nc, *c;
	nclients = 0;
	next_alarm = INT_MAX;
	all_asleep = true;
	nc = &clients;
	while (*nc) {
		c = *nc;
		if (!c->finished) {
			if (!c->awake && clock_val >= c->alarm) {
				wake_up(c);
			}
			if (c->awake)
				all_asleep = false;
			else if (c->alarm < next_alarm)
				next_alarm = c->alarm;
		}
		if (c->finished) {
			*nc = c->next;
			close(c->conn);
			free(c);
			continue;
		}
		poll_input(nclients, c->conn);
		nclients++;
		nc = &c->next;
	}
}

void
update_clock(void)
{
	struct timespec tv;
	if (clock_gettime(CLOCK_BOOTTIME, &tv))
		die("couldn't access clock:");
	clock_val = tv.tv_sec;
}

void
wake_up(struct Client *c)
{
	char buf[BUF_SIZE];
	int n, nw, p;
	n = snprintf(buf, sizeof(buf), "%11ld\n", clock_val);
	p = 0;
	while (p < n) {
		nw = write(c->conn, buf + p, n - p);
		if (nw < 0) {
			if (errno == EINTR)
				continue;
			else if (errno == EAGAIN || errno == EWOULDBLOCK)
				drop_client(c, "too many unread messages.");
			else
				drop_client(c, strerror(errno));
			return;
		}
		p += nw;
	}
	c->alarm = 0;
	c->awake = true;
}

void
write_pid(int pidfile)
{
	char buf[256];
	char *bp;
	ssize_t ws;
	int n;
	n = snprintf(buf, sizeof(buf), "%d\n", getpid());
	assert((size_t) n < sizeof(buf));
	bp = buf;
	while (n > 0) {
		ws = write(pidfile, bp, n);
		if (ws < 0)
			die("can't write pidfile: %s:", pid_path);
		bp += ws;
		n -= ws;
	}
}
