#define _DEFAULT_SOURCE
#include <errno.h>
#include <poll.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>

#include "arg.h"
#define SLEEPD_IMPLEMENT_HERE
#include "sleepd.h"

void	die(const char*, ...);
long	parse_time(const char*);
void	usage(void);

char *argv0;

void
die(const char *fmt, ...)
{
	va_list ap;
	
	va_start(ap, fmt);
	vfprintf(stderr, fmt, ap);
	va_end(ap);
	
	if (fmt[0] && fmt[strlen(fmt)-1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	}
	sleepd_close();
	exit(1);
}

int
main(int argc, char *argv[])
{
	const char *name = NULL;
	bool pipemode = false;
	long t;
	time_t target;
	
	ARGBEGIN {
	case 'n':
		name = EARGF(usage());
		break;
	case 'p':
		pipemode = true;
		break;
	default:
		usage();
	} ARGEND
	
	if (pipemode) {
		if (argc != 0)
			usage();
	} else {
		if (argc != 1)
			usage();
		target = parse_time(argv[0]);
		if (target < 0)
			usage();
	}
	
	if (!name)
		name = pipemode ? "sleepd-until" : "sleepd-until";
	
	if (sleepd_open() || sleepd_introduce(name))
		die("sleepd:");
	
	for (;;) {
		if (pipemode) {
			char buf[16];
			if (!fgets(buf, sizeof(buf), stdin)) {
				if (feof(stdin))
					break;
				die("sleepd-until:");
			}
			target = parse_time(buf);
			if (target < 0)
				die("invalid input time: %s\n", buf);
		}
		if (sleepd_until(0))
			die("sleepd:");
		t = sleepd_read_time();
		if (t < 0)
			die("sleepd: %s\n", strerror(-t));
		sleepd_until(t + target - time(NULL));
		sleepd_read_time();
		if (!pipemode)
			break;
	}
	sleepd_close();
	return 0;
}

long
parse_time(const char *s)
{
	char *end;
	long t;
	t = strtol(s, &end, 10);
	if (*end != '\n' && *end != 0)
		return -1;
	return t;
}

void
usage(void)
{
	fprintf(stderr, "usage: sleepd-until unix_time\n");
	fprintf(stderr, "       sleepd-until -p\n");
	exit(1);
}
